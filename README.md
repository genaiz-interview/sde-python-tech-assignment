# Senior Data Engineer Technical Assignment

# Instructions

The objective of this assignment is to evaluate your coding/problem solving skills. The code must be written in Python, but you are free to use whatever third-party libraries you want.

Please, take the time to structure your code for extensibility and modularity. Here's a list of qualities we are looking for:

- Readability
- Maintainability / extensibility
- Structure
- Functionality
- Documentation
- Testing

# Problem description

In order to test the correlation between certain patient behaviours and cardiovascular diseases, we extracted a large number of data from multiple data sources and merged it into `patients_data.csv`. During the process, some entries have been corrupted or duplicated.

# The assignment

We need you to help us perform the following operations:

1. Remove all duplicated and invalid rows (missing values) from the dataset.

2. Store the removed data for future analysis.

3. Log the total count of removed rows.

Next, help us answer the following questions:

1. How does drinking alcohol and smoking affect a patient's cardiovascular health?

2. How does the cholesterol level of a patient affect their cardiovascular health?

3. How many overweight patients with cholesterol levels well above normal have cardiovascular diseases?

# Datasset Description

| Column      | Description                                                                      |
| ----------- | -------------------------------------------------------------------------------- |
| id          | Index                                                                            |
| age         | in days                                                                          |
| age         | in years                                                                         |
| gender      | (1 = women, 2 = men)                                                             |
| height      | cm                                                                               |
| weight      | kg                                                                               |
| ap_hi       | (Systolic blood pressure)                                                        |
| ap_lo       | (Diastolic blood pressure)                                                       |
| cholesterol | (1: normal, 2: above normal, 3: well above normal)                               |
| gluc        | (1: normal, 2: above normal, 3: well above normal)                               |
| smoke       | Binary feature ; Whether patient smokes (0 = no, 1 = yes)                        |
| alco        | Binary feature ; Whether patient drinks alcohol (0 = no, 1 = yes)                |
| active      | Binary feature (0 = passive life, 1 = active life)                               |
| cardio      | Target variable ; Whether patient has a cardiovascular disease (0 = no, 1 = yes) |

## Body Mass Index

Use BMI (Body Mass Index) to calculate a patients's body fat. The formula is `BMI = kg/m2`, where `kg` is a person's weight in kilograms and `m2` is their height in metres squared. Based on the BMI, we can categorize the patients into the following:

| BMI           | Categories           |
| ------------- | -------------------- |
| Underweight   | <18.5                |
| Normal weight | 18.5–24.9            |
| Overweight    | 25–29.9              |
| Obesity       | BMI of 30 or greater |

# Submission

Create a public repository in github / gitlab / bitbucket and push your code alongside a README.md file with clear instructions on how to set up the environment and run your code. Once you are done, send the link back to the interviewer who assigned you the challenge. An acknowledgment will be sent to you after we receive the solution.

**Please, don't use "uni3t" or "genaiz" in the repository name or description**
